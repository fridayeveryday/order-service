package com.pvl.order_service.domain.models;

import lombok.Getter;

import java.util.List;
import java.util.Objects;

@Getter
public class Item {
    private String title;
    private String description;
    private List<String> mediaLinks;
    private int cost;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Item item = (Item) o;
        return Objects.equals(title, item.title) && Objects.equals(description, item.description) && Objects.equals(mediaLinks, item.mediaLinks);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, description, mediaLinks);
    }
}
