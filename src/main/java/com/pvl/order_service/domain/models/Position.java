package com.pvl.order_service.domain.models;

import com.pvl.order_service.domain.exceptions.NegativeCountException;
import lombok.Getter;

import java.util.Objects;

@Getter
public class Position {
    private Item item;
    private int count;

    public int increaseCount() {
        return ++count;
    }

    public int decreaseCount() {
        if (count - 1 < 0) {
            count = 0;
        }
        return count;
    }

    public int setCount(int count) throws NegativeCountException {
        if (count < 0) {
            throw new NegativeCountException();
        }
        this.count = count;
        return this.count;
    }

    public int getTotalPrice(){
        return count * item.getCost();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Position position = (Position) o;
        return count == position.count && Objects.equals(item, position.item);
    }

    @Override
    public int hashCode() {
        return Objects.hash(item, count);
    }
}
