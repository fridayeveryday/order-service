package com.pvl.order_service.domain.models;

import lombok.Data;
import lombok.Getter;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Getter
public class Order {
    private long orderId;
    private LocalDateTime dateTimeCreation;
    private List<Position> positions;
    private long userId;


    public Order(List<Position> positions) {
        this.dateTimeCreation = LocalDateTime.now();
        this.positions = positions;
    }

    public int getOrderPrice() {
        return positions.stream().mapToInt(Position::getTotalPrice).sum();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return Objects.equals(dateTimeCreation, order.dateTimeCreation) && Objects.equals(positions, order.positions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dateTimeCreation, positions);
    }

    public void addNewPosition(Position newPosition) {
        this.positions.add(newPosition);
    }

    public void removeExistPosition(Position existPosition) {
        this.positions.remove(existPosition);
    }
}
