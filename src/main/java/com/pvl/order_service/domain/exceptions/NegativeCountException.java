package com.pvl.order_service.domain.exceptions;

public class NegativeCountException extends Throwable {
    public NegativeCountException() {
    }

    public NegativeCountException(String message) {
        super(message);
    }

    public NegativeCountException(String message, Throwable cause) {
        super(message, cause);
    }

    public NegativeCountException(Throwable cause) {
        super(cause);
    }

    public NegativeCountException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
