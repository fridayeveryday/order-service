package com.pvl.order_service.infrastructure.repositories;

import com.pvl.order_service.infrastructure.models.OrderEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;

public interface OrderEntityRepository extends JpaRepository<OrderEntity, Long> {
    OrderEntity findByOrderId(Long orderId);

    OrderEntity findByDateTimeCreationBetween(LocalDateTime begin, LocalDateTime end);
}
