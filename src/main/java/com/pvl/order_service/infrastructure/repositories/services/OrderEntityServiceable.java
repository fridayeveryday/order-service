package com.pvl.order_service.infrastructure.repositories.services;

import com.pvl.order_service.infrastructure.models.OrderEntity;

public interface OrderEntityServiceable {
    OrderEntity loadOrderEntity(long id);

    OrderEntity saveOrderEntity(OrderEntity orderEntity);

    void deleteOrderEntity(OrderEntity orderEntity);

    void updateOrderEntity(OrderEntity orderEntity);
}
