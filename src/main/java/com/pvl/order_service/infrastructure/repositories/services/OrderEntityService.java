package com.pvl.order_service.infrastructure.repositories.services;

import com.pvl.order_service.infrastructure.models.OrderEntity;
import com.pvl.order_service.infrastructure.repositories.OrderEntityRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class OrderEntityService implements OrderEntityServiceable {
    private OrderEntityRepository repository;

    @Override
    public OrderEntity loadOrderEntity(long id) {
        return repository.findByOrderId(id);
    }

    @Override
    public OrderEntity saveOrderEntity(OrderEntity orderEntity) {
        return repository.save(orderEntity);
    }

    @Override
    public void deleteOrderEntity(OrderEntity orderEntity) {
        repository.delete(orderEntity);
    }

    @Override
    public void updateOrderEntity(OrderEntity orderEntity) {
        repository.save(orderEntity);
    }
}
