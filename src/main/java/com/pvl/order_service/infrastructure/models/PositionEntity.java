package com.pvl.order_service.infrastructure.models;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;
import lombok.Data;

@Entity
@Data
public class PositionEntity {
    @Id
    private Long id;
    @OneToOne
    private ItemEntity item;
    private int count;
}
