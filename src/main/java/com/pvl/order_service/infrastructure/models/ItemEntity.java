package com.pvl.order_service.infrastructure.models;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import lombok.Data;

import java.util.List;

@Entity
@Data
public class ItemEntity {
    @Id
    private Long id;
    private String title;
    private String description;
    @OneToMany
    private List<String> mediaLinks;
    private int cost;

}
