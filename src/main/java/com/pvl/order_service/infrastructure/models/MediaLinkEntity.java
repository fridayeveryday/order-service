package com.pvl.order_service.infrastructure.models;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.Data;

@Data
@Entity
public class MediaLinkEntity {
    @Id
    private Long id;
    private String link;
}
