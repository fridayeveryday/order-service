package com.pvl.order_service.infrastructure.models;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Entity
@Data
public class OrderEntity {
    @Id
    private long orderId;
    private LocalDateTime dateTimeCreation;
    @OneToMany
    private List<PositionEntity> positions;
    private long userId;
}
